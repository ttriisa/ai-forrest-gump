import {Sprite} from 'pixi.js'
import {Shared} from './index'

export default function SpriteFactory() {
    this.spritePool = []

    this.getSprite = function() {
        if(this.spritePool.length) {
            let sprite = this.spritePool.pop()
            sprite.visible = true
            sprite.texture = Shared.pixiApp.loader.resources.chrBlue.texture
            //Shared.pixiApp.stage.addChild(sprite)
            return sprite
        } else {
            return this.createSprite()
        }
    }

    this.releaseSprite = function(sprite) {
        Shared.overLayer.removeChild(sprite);
        Shared.underLayer.removeChild(sprite);
        sprite.visible = false
        //Shared.pixiApp.stage.removeChild(sprite)
        this.spritePool.push(sprite)
    }

    this.createSprite = function() {
        let sprite = new Sprite(Shared.pixiApp.loader.resources.chrBlue.texture)
        //Shared.pixiApp.stage.addChild(sprite)
        return sprite
    }
}