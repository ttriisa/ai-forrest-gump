import {subjectDefaults, roomParameters, roomCheckpointOrder} from './constants'
import {Shared, Options} from './index'
import {SoftSign} from './helpers'
import * as tf from '@tensorflow/tfjs';

export default function Subject(genes) {
    if(typeof genes === "undefined") {
        this.genes = [
            tf.randomNormal([5,5]),
            //tf.randomNormal([5]),
            //tf.randomNormal([5,5]),
            //tf.randomNormal([5]),
        ];
    } else {
        this.genes = genes;
    }

    this.spawnX = roomParameters[Shared.currentRoom][0];
    this.spawnY = roomParameters[Shared.currentRoom][1];

    this.topSubject = false;
    this.isDead = false;
    this.energy = subjectDefaults.baseEnergy * Options.subjectEnergyModifier;
    this.timeAlive = 0;

    this.checkpointStack = [];
    this.getLastCheckpoint = () => this.checkpointStack[this.checkpointStack.length-1];
    this.backwards = Options.backwards//getDirection()

    this.cachedScore = 0
    this.lapsCompleted = 0

    this.isTextureUpdated = false;
    this.pixiInstance = Shared.SpriteFactory.getSprite()
    this.pixiInstance.x = this.spawnX
    this.pixiInstance.y = this.spawnY
    this.pixiInstance.rotation = roomParameters[Shared.currentRoom][2]
    this.pixiInstance.anchor.x = 0.5
    this.pixiInstance.anchor.y = 0.5
    this.pixiInstance.alpha = 1
    this.pixiInstance.isSubject = true

    if(this.backwards) {
        this.pixiInstance.rotation += 3.14;
        if(this.pixiInstance.rotation < 0)
            this.pixiInstance.rotation = 6.2831 + this.pixiInstance.rotation; //Normalize dir
    }
    this.updateTexture = () => this.pixiInstance.texture = Shared.pixiApp.loader.resources["chr"+(this.backwards?"Light":"")+"Blue"+(this.topSubject?"Top":"")].texture;
    
    this.move = function(delta) {
        if (this.isDead) return;
        if (!this.isTextureUpdated) {
            this.isTextureUpdated = true;
            this.pixiInstance.zOrder = this.topSubject?1:0;
            this.updateTexture();
        }

        this.energy -= delta
        this.timeAlive += delta;

        // Move subject forward
        this.pixiInstance.x += Math.cos(this.pixiInstance.rotation) * subjectDefaults.speed * delta;
        this.pixiInstance.y += Math.sin(this.pixiInstance.rotation) * subjectDefaults.speed * delta;
        
        let sensorData = this.getSensorData();

        // Check vitals
        if (this.energy < 0 || this.detectCollision(sensorData)) {
            return this.kill();
        }

        this.lookForCheckpointColor(); // Collect data for fitness score

        // Get direction change from brain
        const sensorTensor = tf.tensor(sensorData, [1, 5]); //.map(x => x / subjectDefaults.sensorMaxDistance)
        Shared.model.weights.forEach((w, i) => {
            w.val.assign(this.genes[i]);
        });
        let dir = tf.tidy(() => SoftSign(Shared.model.predict(sensorTensor).sum().dataSync()));
        sensorTensor.dispose();

        // I'm tired of them making 180 turns
        if(Math.abs(dir) > 1.5708) {
            return this.kill();
        }

        // Show subject's new direction on canvas
        this.pixiInstance.rotation += dir * subjectDefaults.turningSpeed * delta;
        if(this.pixiInstance.rotation < 0)
            this.pixiInstance.rotation = 6.2831 + this.pixiInstance.rotation; //Normalize dir
    }.bind(this)

    /**
     * TODO separate fitness score and laps count?
     */
    this.calculateFitnessScore = function() {
        let checkpointScore = 0
        let currentRoomOrder = roomCheckpointOrder[Shared.currentRoom]
        //if(Options.debugFitness) console.log("NEW RUN checkpointStack size", this.checkpointStack.length,"direction",backwards ? 'backwards' : 'forward')
        this.lapsCompleted = 0
        for(let i=1; i<this.checkpointStack.length; i++) {
            let limitOverflow = i;
            if(this.backwards) limitOverflow += currentRoomOrder.length-1;
            while(limitOverflow >= currentRoomOrder.length) {
                limitOverflow -= currentRoomOrder.length
            }
            if(this.backwards) {
                limitOverflow = currentRoomOrder.length-1 - limitOverflow
            }
            
            if(typeof currentRoomOrder[limitOverflow] !== "undefined") {
                if(Options.debugFitness) console.log("comparing colors i=",i,"limitOverflow=",limitOverflow,"should be=",currentRoomOrder[limitOverflow],"color is=",this.checkpointStack[i],currentRoomOrder[limitOverflow] === this.checkpointStack[i])
                if(currentRoomOrder[limitOverflow] === this.checkpointStack[i]) {
                    if(limitOverflow === 0) this.lapsCompleted++;
                    checkpointScore += 800 + i*200 + this.energy/10
                    if(Options.debugFitness) console.log("added", 500 + i*100 + this.energy/10,"score, total",checkpointScore)
                } else {
                    if(Options.debugFitness) console.log("added 0, terminating")
                    checkpointScore = 0.0000000001 //punish going in wrong direction
                    // maybe also kill?
                    break;
                }
            }
        }

        // For starters find max distance from spawn
        if(checkpointScore === 0)
            checkpointScore = Math.sqrt(Math.abs(Math.pow(this.pixiInstance.x-this.spawnX,2)+Math.pow(this.pixiInstance.y-this.spawnY,2))) / 2;

        this.cachedScore = this.timeAlive/100 + checkpointScore
        return this.cachedScore
    }.bind(this)

    /**
     * Breed two subject by crossovering genes
     * @param {Subject} otherSubject 
     * @param {number} bias Bias increases gene base pick chance towards this (calling) subject
     * @returns {Subject}
     */
    this.breed = (otherSubject, bias) => {
        const newGenes = [];
        for (const key in this.genes) {
            if (this.genes.hasOwnProperty(key) && otherSubject.genes.hasOwnProperty(key)) {
                newGenes[key] = geneCrossover(this.genes[key], otherSubject.genes[key], bias);
            }
        }
        return new Subject(newGenes);
    }

    this.mutate = () => {
        this.genes = this.genes.map(gene => {
            let buffer = gene.bufferSync();
            for (let i = buffer.values.length-1; i > 0; i--) {
                if(Math.random() < Options.mutationRate) {
                    buffer.values[i] += (Math.random()*Options.mutationStrength*2)-Options.mutationStrength
                }
            }
            return buffer.toTensor();
        });
        return this;
    }

    this.clone = () => new Subject(this.cloneGenes());
    this.cloneGenes = () => this.genes.map(g => g.clone());

    this.geneString = () => {
        let str = "";
        this.genes.forEach(gene => {
            gene.dataSync().forEach(base => str += (base >= 0 ? "  " : " ") + base.toFixed(2))
            str+="\n";
        });
        return str;
    }

    this.genesArray = () => this.genes.map(g => g.arraySync());

    this.kill = function() {
        this.isDead = true
        if(this.pixiInstance) {
            this.pixiInstance.texture = Shared.pixiApp.loader.resources["chrRed"+(this.topSubject?"Top":"")].texture
            this.pixiInstance.alpha = 0.2
        }
        Shared.deadSubjectsCount++;
    }.bind(this)

    this.dispose = () => this.genes.forEach(g => g.dispose());
    
    this.rotate = function(dir) {
        if(this.pixiInstance) {
            this.pixiInstance.rotation += dir
            
            //Normalize dir
            if(this.pixiInstance.rotation < 0) this.pixiInstance.rotation = 6.2831 + this.pixiInstance.rotation
        }
    }.bind(this)

    this.setRotation = function(dir) {
        if(this.pixiInstance)
            this.pixiInstance.rotation = dir
    }.bind(this)
    
    this.getSensorData = () => [
        this.sensorWatchDir(-1.5708), //90deg left
        this.sensorWatchDir(-0.7854), //45deg left
        this.sensorWatchDir(      0),
        this.sensorWatchDir( 0.7854),
        this.sensorWatchDir( 1.5708),
    ];
    
    //Direction is relative to sprite rotation
    this.sensorWatchDir = (dir) => {
        //Normalize dir
        let currentDir = this.pixiInstance.rotation + dir
        if(currentDir < 0) currentDir = 6.2831 + currentDir
        
        //Speed up raytrace by moving 2 px every time as distance accuracy isn't so important and start from edge of subject itself
        for(let distance = 12; distance < subjectDefaults.sensorMaxDistance; distance+=2) {
            let x = Math.floor(this.pixiInstance.x + Math.cos(currentDir) * distance)
            let y = Math.floor(this.pixiInstance.y + Math.sin(currentDir) * distance)
            let pixelPos = ((y * subjectDefaults.bgImageRowWidthPx) + x)*4;
            
            // Checking color out of bounds is considered as a wall
            if(typeof(Shared.roomPixelData[pixelPos]) === "undefined") {
                return distance;//{x, y, dist: distance}
            }
            
            // Check the background image color to detect a wall
            let color = Shared.roomPixelData[pixelPos+2] | (Shared.roomPixelData[pixelPos+1] << 8) | (Shared.roomPixelData[pixelPos] << 16);
            //let hexStr = '#' + (0x1000000 + color).toString(16).slice(1)
            if(color === subjectDefaults.wallColor) {
                return distance;//{x, y, dist: distance}
            }
        }

        // Return max sensing distance
        const maxX = Math.floor(this.pixiInstance.x + Math.cos(currentDir) * subjectDefaults.sensorMaxDistance)
        const maxY = Math.floor(this.pixiInstance.y + Math.sin(currentDir) * subjectDefaults.sensorMaxDistance)
        return subjectDefaults.sensorMaxDistance;//{x: maxX, y: maxY, dist: subjectDefaults.sensorMaxDistance}
    }

    this.lookForCheckpointColor = () => {
        let x = Math.floor(this.pixiInstance.x)
        let y = Math.floor(this.pixiInstance.y)
        //console.log(Shared.roomPixelData); throw new Error("test")
        let pixelPos = ((y * subjectDefaults.bgImageRowWidthPx) + x)*4
        if(Shared.roomPixelData[pixelPos] !== "undefined") {
            let color = Shared.roomPixelData[pixelPos+2] | (Shared.roomPixelData[pixelPos+1] << 8) | (Shared.roomPixelData[pixelPos] << 16)
            //console.log(pixelPos, color, '#' + (0x1000000 + color).toString(16).slice(1))
            if(isCheckpointColor(color) && this.getLastCheckpoint() !== color) {
                
                //console.log("got checkpoint")
                this.checkpointStack.push(color)
                //obj.energy += 100
                //if(Options.debugFitness) console.log("got checkpoint, score",obj.getFitnessScore())
            }
        }
    }

    this.detectCollision = (sensorData) => {
        //let sensorMin = subjectDefaults.sensorMaxDistance;
        for(let i=0,sensor; sensor = sensorData[i]; i++) {
            //if(sensor.dist < sensorMin)
            //    sensorMin = sensor.dist
            if(sensor <= subjectDefaults.collisionDistance)
                return true;
        }
        return false;
    }
}

/**
 * Go through all bases in both genes and randomly take one or another
 * @param {tf.Tensor} geneA
 * @param {tf.Tensor} geneB
 * @param {number} bias Bias increases geneA base pick chance over geneB
 * @returns {tf.Tensor}
 */
const geneCrossover = (geneA, geneB, bias=0) => {
    geneA = geneA.bufferSync();
    geneB = geneB.bufferSync();
    const combinedGene = tf.buffer(geneA.shape);
    for(let i=0, baseA, baseB; baseA = geneA.values[i], baseB = geneB.values[i]; i++) {
        combinedGene.values[i] = (Math.random() < (0.5+bias)) ? baseA : baseB;
    }
    return combinedGene.toTensor();
}

function isCheckpointColor(color) {
    return roomCheckpointOrder[Shared.currentRoom].indexOf(color) !== -1
}