export const subjectDefaults = {
    speed: 2.1,
    turningSpeed: 0.5,
    sensorMaxDistance: 255,
    collisionDistance: 16,
    wallColor: 0x848484,
    baseEnergy: 2000,
    bgImageRowWidthPx: 800,
}
const facing = {
    right: 0,
    down: 1.5708,
    left: 1.5708*2,
    up: 1.5708*3,
}
// spawnX, spawnY, facing (rad), energy (ticks)
export const roomParameters = [
    [ 74, 301, facing.up,    2000],
    [ 88, 274, facing.up,    2200],
    [ 99, 274, facing.up,    2200],
    [675, 312, facing.up,    2000],
    [721, 300, facing.up,    3000],
    [269, 350, facing.right, 3600],
    [718, 300, facing.up,    2000],
    [400, 121, facing.right, 1200],
    [582, 300, facing.down,  1200],
    [582, 300, facing.up,    1200],
    [400, 121, facing.left,  1200],
]
export const roomCheckpointOrder = [
    [0xffff00,0x00ff0c],
    [0xffff00,0x00ff0c,0x00fffc],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff],
    [0xffff00,0x00ff0c,0x00fffc],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
    [0xffff00,0x00ff0c,0x00fffc,0xff00ff,0xff8000],
]