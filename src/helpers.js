import {Shared} from './index'
import {roomParameters, subjectDefaults} from './constants'

export function leakyReLU(x) {
	return Math.max(0,x) - 0.01 * Math.max(0,-x)
}

export function SoftSign(x)
{
	return x / (1 + Math.abs(x));
}

export function percentage(arr) {
	let percentages = []
	let sum = 0
	for(let i=0; i<arr.length; i++)
		sum += arr[i];
	for(let i=0; i<arr.length; i++) {
		percentages.push(arr[i]/sum)
	}
	return percentages
}

export function normalize(arr) {
	let normalized = [];
	let min = Number.MAX_SAFE_INTEGER;
	let max = Number.MIN_SAFE_INTEGER;
	for(let i=arr.length-1; i>=0; i--) {
		if(arr[i] < min) min = arr[i];
		if(arr[i] > max) max = arr[i];
	}

	for(let i=0; i<arr.length; i++) {
		normalized.push((arr[i] - min) / (max - min))
	}
	return normalized
}


/**
 * Returns a random number between min (inclusive) and max (exclusive)
 */
export function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
export function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function pickRandomBasedWeight(arr) {
	let index = 0
	let random = Math.random()
	while(random > 0) {
		random -= arr[index]
		index++
	}
	return --index
}

export function setRoom(roomId) {
	Shared.currentRoom = roomId;
	subjectDefaults.baseEnergy = roomParameters[Shared.currentRoom][3];
	Shared.roomSprite.texture = Shared.pixiApp.loader.resources["room"+Shared.currentRoom].texture;
	Shared.roomPixelData = Shared.renderer.plugins.extract.pixels(Shared.roomSprite);
}

/**
 * State is set immediately, but localStorage writes are delayed up to 10 seconds to reduce SSD wear
 * @param name
 * @return {*[]}
 */
export function useLocalStorageState(name, defaultValue={}) {
    let timerId, lastCheck = (new Date).getTime(), lastCall = 0;
    const [state, setState] = useState(JSON.parse(localStorage.getItem(name) || "null") || defaultValue);
    return [
        state,
        (newValue, key = null, callback, delay = 10000) => {
            let newState = setState(newValue, key, callback);
            const now = (new Date).getTime();
            if (!timerId)
                lastCall = now;
            if (now - lastCheck < delay)
                clearTimeout(timerId);
            timerId = setTimeout(() => {
                try {
                    localStorage.setItem(name, JSON.stringify(newState));
                } catch (e) {
                    // localStorage is not available
                }
                timerId = null;
                lastCall = (new Date).getTime();
            }, (delay - (now - lastCall)) || 0);
            lastCheck = now;
        }
    ];
}

/**
 * React-like state with special ability to directly set object values using key
 * [obj, setObj] = useState();
 * setObj(10, "six");
 * // console.log(obj) results {"six": 10}
 * @param {*} defaultValue
 * @returns {*[]}
 */
export function useState(defaultValue) {
	let state = defaultValue || {};
	return [state, (newValue, key, callback) => {
		if (key !== null && typeof state === "object") {
			state[key] = newValue;
		} else {
			state = newValue;
		}
		if (typeof callback === "function")
			callback(state);
		return state;
	}];
}

export function createSlider({min=1, max=100, step=1, value, labelFn=(v)=>v, onChange}) {
	let container = document.createElement("div");
	container.style.border = "1px solid #999";
	container.style.padding = "0.3rem";
	container.style.marginBlockEnd = "0.2rem";
	let label = document.createElement("label");
	label.style.display = "block";
    label.innerHTML = labelFn(parseInt(value));
	let slider = document.createElement("input");
	slider.style.width = "20rem";
    slider.type="range";
    slider.defaultValue=value;
    slider.value=value;
    slider.step=step;
    slider.min=min;
    slider.max=max;
    slider.addEventListener("change", (e) => {
        let value = parseInt(e.target.value);
		label.innerHTML = labelFn(value);
		onChange(value);
	});

	let isDown = false;
	slider.addEventListener("mousedown", (e) => {
        isDown = true;
	});
	slider.addEventListener("mousemove", (e) => {
		if (isDown) {
			let value = parseInt(slider.value);
			label.innerHTML = labelFn(value);
			onChange(value);
		}
	});
	slider.addEventListener("mouseup", (e) => {
        isDown = false;
	});
    container.appendChild(label);
	container.appendChild(slider);
	appendToToolbar("toolConfigSliders", container);
}

export const geneCrossoverDemo = (bias=0) => {
	let str = "";
    for(let i=0; i<16; i++) {
		if(i===12) str+=" ";
        str += (Math.random() < (0.5+bias)) ? "s" : "W";
    }
    return str;
}

export const clearTextOverlay = () => Shared.overlayContainer.innerHTML = "";

export function showLargeText(text, time=4000) {
	let largeText = document.createElement("div");
    largeText.style.fontSize = "4em";
    largeText.style.color = "#fff";
    largeText.style.webkitTextStroke = "0.03em #000";
    largeText.style.fontFamily = "Comic Sans MS";
    largeText.innerHTML = text;
	Shared.overlayContainer.appendChild(largeText);
	setTimeout(() => largeText.remove(), time);
}

let isFirstToolAdded = false;
export function createToolbarElement(id, label) {
	let container = document.createElement("div");
	container.id = id;
	container.className = "tool-container";
	container.style.overflowY = "auto";
	if (!isFirstToolAdded)
		isFirstToolAdded = true;
	else
		container.style.display = "none"
	Shared.tools[id] = container;
	Shared.toolbar.appendChild(container);

	let tab = document.createElement("div");
	tab.innerHTML = label;
	tab.style.border = "1px solid #999";
	tab.style.color = "#bbb";
	tab.style.cursor = "pointer";
	tab.style.display = "inline-block";
	tab.onclick = (e) => {
		[...document.querySelectorAll(".tool-container")].forEach(t => t.style.display = "none");
		container.style.display = "block";
		//tab.style.border = "1px solid #ccc";
		//tab.style.color = "#eee";
	};
	Shared.toolbarTabs.appendChild(tab);
}

export const appendToToolbar = (id, item) => Shared.tools[id].appendChild(item);
export const clearToolbar = (id) => Shared.tools[id].textContent="";