import * as PIXI from 'pixi.js';
import SpriteFactory from './SpriteFactory';
import Subject from './Subject';
import * as tf from '@tensorflow/tfjs';
import Stats from 'stats.js';
import {pickRandomBasedWeight, createSlider, getRandomInt, setRoom, useLocalStorageState, geneCrossoverDemo,
    showLargeText, clearTextOverlay, createToolbarElement, appendToToolbar, clearToolbar} from './helpers';
import {roomParameters} from './constants'

let pixiApp,statsDelay;
let allTimeBestScore = 0;

// TODO add toggleable buttons for gene bank: breeding, just next run, favorites
// TODO check backwards lap counting, something seems off (room3)
// TODO show subject brain and compare to last best/all time best (modes: weights, real time firing, compare)
// TODO track gene pool changes and total gene pool similarity
// TODO stats how many alive, avg score etc
// TODO checkboxes and textboxes for changing training params
// TODO first layer is controlled by brain, second is genetic - like limbic system
// TODO extra colors and features based on mutation, new genome object separate weights from other stuff, named genes

const [options, setOptions] = useLocalStorageState("options", {
    populationLimit: 30,
    debugFitness: false,
    backwards: false,

    /* Auto train */
    autoTrain: true,
    allowAutoMapChange: true,
    autoMapChangeRate: 0.05,
    allowAutoDirectionChange: true,
    autoDirectionChangeRate: 0.2,

    /* Learning rates */
    mutationRate: 0.05,
    mutationStrength: 0.3,
    crossoverExtraBias: 0.3,
    subjectEnergyModifier: 1,
});
export const Options = options

export const Shared = {
    model: tf.sequential({
        layers: [
            tf.layers.dense({inputShape: [5], units: 5, activation: 'relu', useBias: false, kernelConstraint: "nonNeg"}),
            //tf.layers.dense({units: 5, activation: 'relu'}),
        ]
    }),

    epoch: 0,
    population: [],
    deadSubjectsCount: 0,
    nextRoom: null,
    addGenomeToNextRun: [],
    addGenomeToNextBreed: [],

    pixiApp,
    SpriteFactory: null,
    currentRoom: 0,
    roomSprite: null,
    renderer: null,
    roomPixelData: null,
    toolConfigSliders: null,
    overlayContainer: null,
    underLayer: null,
    overLayer: null,
    toolbar: null,
    toolbarTabs: null,
    tools: {},
};
window.Shared = Shared;

//#region Gene Bank
let [geneBank, setGeneBank] = useLocalStorageState("geneBank", []);
window.geneBank = geneBank;
//export const GeneBank = geneBank;
const geneBankCallback = () => {
    //clearToolbar("geneBank");
    let table = document.querySelector("#geneBankTable");
    if (table) {
        table.innerHTML = "";
    } else {
        let title = document.createElement("p");
        title.textContent = "Top 100 genes";
        appendToToolbar("geneBank", title);

        let addSelected = document.createElement("button");
        addSelected.textContent = "Add selected to the pool";
        addSelected.onclick = () => {
            console.log("Adding selected to the pool");
            geneBank.forEach(g => {
                if (g.checked && !Shared.addGenomeToNextRun.includes(g)) {
                    Shared.addGenomeToNextRun.push(g);
                    geneBankCallback();
                }
            })
        };
        appendToToolbar("geneBank", addSelected);

        table = document.createElement("table");
        table.id = "geneBankTable";
        table.style.minWidth = "20rem";
        appendToToolbar("geneBank", table);
    }
    geneBank.forEach(genome => {
        let row = document.createElement("tr");
        let name = document.createElement("td");

        let select = document.createElement("td");
        let checkBox = document.createElement("input");
        checkBox.type="checkbox";
        if (genome.checked)
            checkBox.checked = genome.checked;
        checkBox.onclick = () => {
            genome.checked = checkBox.checked;
            if (name.style.color !== "orange")
                name.style.color = checkBox.checked?"lime":"#eee";
        }
        select.appendChild(checkBox);
        row.appendChild(select);
        
        name.textContent = genome.name;
        if (Shared.addGenomeToNextRun.includes(genome))
            name.style.color = "orange";
        else if (checkBox.checked)
            name.style.color = "lime";
        name.onclick = (e) => {
            console.log(genome)
        };
        row.appendChild(name);

        let score = document.createElement("td");
        score.textContent = genome.score.toFixed(0);
        row.appendChild(score);

        let actions = document.createElement("td");
        
        row.appendChild(actions);

        table.appendChild(row);
    });
}
const appendGeneBank = (genome) => {
    console.log("appendGeneBank");
    geneBank.unshift(genome);
    geneBank = geneBank.filter((g,i) => g.checked || i < 101);
    setGeneBank(geneBank, null, geneBankCallback,8000);
}
//#endregion

window.addEventListener("DOMContentLoaded", function () {
    pixiApp = new PIXI.Application({
        width: 800,
        height: 600,
        antialias: true,
        transparent: false,
        resolution: 1,
        preserveDrawingBuffer: true
    });
    Shared.pixiApp = pixiApp;
    
    Shared.renderer = PIXI.autoDetectRenderer(800, 600);
    //canvas = renderer.view;
    //ctx = renderer.context;
    //gl = renderer.gl;

    //#region HTML containers
    document.body.style.color = "#eee";
    document.body.style.backgroundColor = "#333";
    let flexContainer = document.createElement("div");
    flexContainer.style.display = "flex";
    let canvasWrap = document.createElement("div");
    canvasWrap.appendChild(pixiApp.view);
    flexContainer.appendChild(canvasWrap);
    document.body.appendChild(flexContainer);

    Shared.toolbar = document.createElement("div");
    Shared.toolbar.style.display = "flex";
    Shared.toolbar.style.flexDirection = "column";
    Shared.toolbar.style.maxHeight = "100vh";
    Shared.toolbarTabs = document.createElement("div");
    Shared.toolbar.appendChild(Shared.toolbarTabs);
    flexContainer.appendChild(Shared.toolbar);

    Shared.overlayContainer = document.createElement("div");
    Shared.overlayContainer.style.display = "flex";
    Shared.overlayContainer.style.position = "fixed";
    Shared.overlayContainer.style.top = "0";
    Shared.overlayContainer.style.left = "0";
    Shared.overlayContainer.style.width = "800px";
    Shared.overlayContainer.style.height = "600px";
    Shared.overlayContainer.style.justifyContent = "center";
    Shared.overlayContainer.style.alignItems = "center";
    document.body.appendChild(Shared.overlayContainer);
    //#endregion

    //#region Tool - Adjust params
    createToolbarElement("toolConfigSliders", "Adjust params");
    createSlider({
        value: Options.mutationRate*100,
        labelFn: (v) => `Mutation rate: ${v}%`,
        onChange: (v) => setOptions(v/100, "mutationRate"),
    });
    createSlider({
        value: Options.mutationStrength*100,
        labelFn: (v) => `Mutation max strenght: ${v} centiunits`,
        onChange: (v) => setOptions(v/100, "mutationStrength"),
    });
    createSlider({
        value: Options.subjectEnergyModifier*100,
        labelFn: (v) => `Subject base energy modifier: ${v}%`,
        onChange: (v) => setOptions(v/100, "subjectEnergyModifier"),
        step: 1,
        min: 15,
        max: 200
    });
    createSlider({
        value: Options.crossoverExtraBias*200,
        labelFn: (v) => `Gene crossover bias towards stronger subject: ${v}% (${v/200})<br>Example gene after breeding strong and weak subjects:<br><pre>`+geneCrossoverDemo(v/200)+"</pre>",
        onChange: (v) => setOptions(v/200, "crossoverExtraBias"),
        step: 1,
        min: 0,
        max: 100
    });
    createSlider({
        value: Options.autoMapChangeRate*100,
        labelFn: (v) => `Auto map change rate: ${v}%`,
        onChange: (v) => setOptions(v/100, "autoMapChangeRate"),
        min: 0,
    });
    createSlider({
        value: Options.autoDirectionChangeRate*100,
        labelFn: (v) => `Auto direction change rate: ${v}%`,
        onChange: (v) => setOptions(v/100, "autoDirectionChangeRate"),
        min: 0,
    });
    //#endregion
    createToolbarElement("geneBank", "Gene Bank");

    statsDelay = new Stats();
    statsDelay.showPanel( 1 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    document.body.appendChild( statsDelay.dom );

    pixiApp.loader
        .add("room0", "images/room0.png")
        .add("room1", "images/room1.png")
        .add("room2", "images/room2.png")
        .add("room3", "images/room3.png")
        .add("room4", "images/room4.png")
        .add("room5", "images/room5.png")
        .add("room6", "images/room6.png")
        .add("room7", "images/room7.png")
        .add("room8", "images/room8.png")
        .add("room9", "images/room9.png")
        .add("room10", "images/room10.png")
        .add("chrRed", "images/character_red.png")
        .add("chrBlue", "images/character_blue.png")
        .add("chrLightBlue", "images/character_lightblue.png")
        .add("chrYellow", "images/character_yellow.png")
        .add("chrBlueTop", "images/character_blue_top.png")
        .add("chrLightBlueTop", "images/character_lightblue_top.png")
        .add("chrRedTop", "images/character_red_top.png")
        .load(onAssetsLoaded);
});

function onAssetsLoaded(loader, resources) {
    Shared.SpriteFactory = new SpriteFactory();

    Shared.roomSprite = new PIXI.Sprite(resources["room"+Shared.currentRoom].texture);
    Shared.roomPixelData = Shared.renderer.plugins.extract.pixels(Shared.roomSprite);
    pixiApp.stage.addChild(Shared.roomSprite);

    Shared.underLayer = new PIXI.Container();
    pixiApp.stage.addChild(Shared.underLayer);

    Shared.overLayer = new PIXI.Container();
    pixiApp.stage.addChild(Shared.overLayer);

    // Render Gene Bank
    geneBankCallback();

    if (!Shared.population.length) {
        createNewPopulation();
    }
    
    pixiApp.stage.interactive = true
    pixiApp.stage.on('mousedown', (event) => {
        let mouseData = event.data.getLocalPosition(pixiApp.stage)
        subject.pixiInstance.x = mouseData.x
        subject.pixiInstance.y = mouseData.y
        console.log(mouseData.x,mouseData.y)
    });
    
    pixiApp.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta) {
    statsDelay.begin();
    
    for (let i=0,subject; subject = Shared.population[i]; i++) {
        subject.move(delta);
        // TODO List current subjects on toolbar
    }

    if (Shared.deadSubjectsCount >= Shared.population.length) {
        removePopulationFromStage();
        savePopulationGenes();

        changeStage();
        breedNewPopulation();
        addPopulationToStage();

        clearTextOverlay();
        showLargeText(`Generation ${Shared.epoch}`);
    }
    statsDelay.end();
}

function createNewPopulation() {
    Shared.epoch = JSON.parse(localStorage.getItem("epoch") || "0");
    let genePool = JSON.parse(localStorage.getItem("genePool") || "[]");
    if (genePool.length) {
        // Move better subject to the top in case populationLimit is smaller than pool
        genePool.sort((a,b) => b.score-a.score);
        console.log("Imported gene pool with highest score of",genePool[0].score);
        allTimeBestScore = genePool[0].score;
    }
    for (let i = 0; i < Options.populationLimit; i++) {
        if (typeof genePool[i] !== "undefined") {
            Shared.population.push(new Subject(genePool[i].genes.map(g => tf.tensor(g))));
        } else {
            Shared.population.push(new Subject());
        }
    }
    addPopulationToStage();
    console.log("Created population with size", Shared.population.length,"Starting epoch", Shared.epoch);
}

function breedNewPopulation() {
    //#region Stats + find the best subject
    let lastRunBestScore = 0;
    let lastRunBestSubject = null;
    let maxScore = 0;
    let passedCount = 0;
    for(let i=0, subject; subject = Shared.population[i]; i++) {
        subject.calculateFitnessScore();
        maxScore += subject.cachedScore;

        if(subject.lapsCompleted >= 2) passedCount++;

        if(subject.cachedScore > allTimeBestScore) {
            allTimeBestScore = subject.cachedScore;
        }

        if(!Shared.addGenomeToNextRun.includes(subject) && subject.cachedScore > lastRunBestScore) {
            lastRunBestSubject = subject;
            lastRunBestScore = subject.cachedScore;
        }
    }

    // Avoid duplicated by adding only mutated or breeded subjects
    if (!lastRunBestSubject.topSubject) {
        appendGeneBank({
            name: "Best of epoch " + Shared.epoch,
            createdAt: new Date().toLocaleString('et'),
            score: lastRunBestScore,
            genes: lastRunBestSubject.genesArray(),
        });
    }

    let passedRate = passedCount / Shared.population.length;
    console.log("Passed rate for last gen were "+(passedRate*100).toFixed(2)+"%",passedCount,Shared.population.length);
    console.log("All time highest", allTimeBestScore, "Last time highest", lastRunBestScore);
    //#endregion

    let newPopulation = [];
    Shared.epoch++;
    Shared.deadSubjectsCount = 0;
    console.log("Breeding generation ",Shared.epoch, "Test", Shared.backwards ? 'backward': 'forward',"in room",Shared.currentRoom)

    // Prepare weighted pool for random breeding picks
    let scorePool = []
    // Add outsiders score to the total score
    if (Shared.addGenomeToNextBreed.length) {
        for(let i=0, genome; genome = Shared.addGenomeToNextBreed[i]; i++) {
            maxScore += genome.score
        }
    }
    
    // Add previous gen subjects to the breeding pool
    for(let i=0, subject; subject = Shared.population[i]; i++) {
        scorePool[i] = subject.cachedScore / maxScore;
    }

    // Add outsiders to the breeding pool
    if (Shared.addGenomeToNextBreed.length) {
        for(let i=0, genome; genome = Shared.addGenomeToNextBreed[i]; i++) {
            scorePool[i] = genome.score / maxScore;
            Shared.population.push(new Subject( genome.genes.map(g => tf.tensor([...g])) ));
        }
    }

    // Add outsiders to the next run, but not for breeding
    if (Shared.addGenomeToNextRun.length) {
        for(let i=0, genome; genome = Shared.addGenomeToNextRun[i]; i++) {
            let subject = new Subject(genome.genes.map(g => tf.tensor(g)));
            subject.topSubject = true;
            newPopulation.push(subject);
        }
    }

    // Always transfer the best subject to the new population
    let lastBestClone = cloneSubject(lastRunBestSubject);
    lastBestClone.topSubject = true;
    newPopulation.push( lastBestClone );

    // Always mutate a clone of the subject
    newPopulation.push( mutateSubject(lastRunBestSubject) );

    // Always breed the best subject with someone else
    for(let i=0; i < 4; i++) {
        let subjectB = pickDifferentSubjectFrom(lastRunBestSubject, scorePool);
        newPopulation.push( breedSubjectsBiasedTowardsStronger(lastRunBestSubject, subjectB) );
    }

    // Breed remaining population randomly
    for(let i=0; i < Options.populationLimit-6-Shared.addGenomeToNextRun.length; i++) {
        let method = breedingMethods[Math.floor(Math.random() * breedingMethods.length)];
        let subject = Shared.population[pickRandomBasedWeight(scorePool)];

        if (method.length === 2) {
            newPopulation.push( method.call(null, subject, pickDifferentSubjectFrom(subject, scorePool)) );
        } else {
            newPopulation.push( method.call(null, subject) );
        }
    }

    Shared.population.forEach(s => s.dispose())
    Shared.population = newPopulation;
}

const breedSubjects = (subjectA, subjectB) => subjectA.breed(subjectB);
const breedSubjectsBiasedTowardsStronger = (subjectA, subjectB) => subjectA.breed(subjectB, Options.crossoverExtraBias * (subjectA.calculateFitnessScore() > subjectB.calculateFitnessScore() ? 1 : -1));
const cloneSubject = (subject) => subject.clone();
const mutateSubject = (subject) => subject.clone().mutate();
const mutateBredSubject = (subjectA, subjectB) => subjectA.breed(subjectB).mutate();
const breedingMethods = [
    breedSubjects,
    breedSubjectsBiasedTowardsStronger,
    cloneSubject,
    mutateSubject,
    mutateBredSubject,
];

function changeStage() {
    if (Shared.nextRoom !== null) {
        setRoom(Shared.nextRoom);
        Shared.nextRoom = null;
    } else if (Options.autoTrain) {
        if (Options.allowAutoMapChange && Math.random() < Options.autoMapChangeRate) {
            let randomRoom = Shared.currentRoom
            while(randomRoom === Shared.currentRoom)
                randomRoom = getRandomInt(0,roomParameters.length-2);
            setRoom(randomRoom);
        } else if(Options.allowAutoDirectionChange && Math.random() < Options.autoDirectionChangeRate) {
            Options.backwards = !Options.backwards;
        }
    }
}

function pickDifferentSubjectFrom(subjectA, scorePool) {
    let maxTriesBeforeRandom = 30;
    let subjectB = Shared.population[pickRandomBasedWeight(scorePool)];
    while(subjectA === subjectB && maxTriesBeforeRandom > 0) {
        maxTriesBeforeRandom--;
        subjectB = Shared.population[pickRandomBasedWeight(scorePool)];
    }
    if(subjectA === subjectB)
        return new Subject();
    return subjectB;
}

function addPopulationToStage() {
    for(let i=0, subject; subject = Shared.population[i]; i++) {
        Shared[subject.topSubject?"overLayer":"underLayer"].addChild(subject.pixiInstance);
    }
}

function removePopulationFromStage() {
    for (let i=0, subject; subject = Shared.population[i]; i++) {
        if(subject.pixiInstance !== null)
            Shared.SpriteFactory.releaseSprite(subject.pixiInstance);
    }
}

function savePopulationGenes() {
    let genePool = [];
    for (let i=0, subject; subject = Shared.population[i]; i++) {
        genePool.push({
            genes: subject.genesArray(),
            score: subject.calculateFitnessScore(),
        });
    }
    localStorage.setItem("genePool", JSON.stringify(genePool));
    localStorage.setItem("epoch", JSON.stringify(Shared.epoch));
}